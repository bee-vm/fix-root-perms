#!/bin/sh
# kFreeBSD do not accept scripts as interpreters, using #!/bin/sh and sourcing.
if [ true != "$INIT_D_SCRIPT_SOURCED" ] ; then
    set "$0" "$@"; INIT_D_SCRIPT_SOURCED=true . /lib/init/init-d-script
fi
### BEGIN INIT INFO
# Provides:          fix-root-perms
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Random root password and enforces lockout
# Description:       This executes a small executable that
#                    randomizes the root password and immediately
#                    locks the root account out to ensure sanity.
### END INIT INFO

# Author: Ashley Stone (BeeVM.net) <ashstone@beevm.net>

DESC="fix-root-perms"
DAEMON=/usr/sbin/fix-root-perms

case "$1" in
start)
	echo Executing fix-root-perms.
        $DAEMON
        ;;
*)   
	echo Nothing todo for fix-root-perms.
        ;;
esac
exit 0
